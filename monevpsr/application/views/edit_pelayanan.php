<?php echo form_open('pelayanan/edit_simpan');  ?>
<?php echo form_hidden('id',$this->uri->segment(3));  ?>    
<html>
<head>
	<title>
	 Form Input
	</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script>

<script type="text/javascript">
	$(function(){

	$.ajaxSetup({
	type:"POST",
	url: "<?php echo base_url('index.php/select/ambil_data') ?>",
	cache: false,
	});

	$("#unor").change(function(){

	var value=$(this).val();
			if(value>0){
				$.ajax({
			data:{modul:'unker',id:value},
		success: function(respond){
	$("#unker").html(respond);
			}
		});
	}
	});
	});

	</script>
</head>
<body>
    
    <div class='container'>

    <div class='row'>

    <div class='col-md-5'>
    
    <h3>Form Input</h3>
    
<table>
    <tr><td>No</td><td><?php echo form_input('idpemohon', $pemohon['idpemohon'],array('placeholder'=>' pemohon')); ?></td></tr>
    <tr><td>Nama Pemohon</td><td><?php echo form_input('nama', $pemohon['nama'],array('placeholder'=>'nama')); ?></td></tr>
    <tr><td>NIP</td><td><?php echo form_input('nip', $pemohon['nip'],array('placeholder'=>'NIP')); ?></td></tr>
    
    <tr><td>        
        <div class='form-group'>
	<label>Unit Organisasi</label>
	<td><select name="idsatminkal" class='form-control' id='unor'>
                <option value='0'>--pilih--</option>

	<?php 
	foreach ($unor as $u1) {
		echo "<option value='$u1[idsatminkal]'>$u1[uraiunker]</option>";
	}
	?>
	</select></td>
	</div>
    </td></tr>        

    <tr><td>
	<div class='form-group'>
	<label>Unit Kerja Eselon 2</label>
	<td> <select name="idunker" class='form-control' id='unker'>
                <option value='0'>--pilih--</option></td>
	</select>
	</div>
    </td></tr>
	</div>
	</div>

	</div>
    
    <tr><td>Eselon 3</td><td><?php echo form_input('ideselon3',$pemohon['ideselon3'],array('placeholder'=>'eselon3')); ?></td></tr>
    <tr><td>Propinsi</td><td><?php echo form_input('idpropinsi', $pemohon['idpropinsi'],array('placeholder'=>'propinsi')); ?></td></tr>
    <tr><td>Aplikasi</td><td><?php echo form_input('idaplikasi', $pemohon['idaplikasi'],array('placeholder'=>'aplikasi')); ?></td></tr>
    <tr><td>Masalah</td><td><?php echo form_input('masalah', $pemohon['masalah'],array('placeholder'=>'masalah')); ?></td></tr>
    <tr><td>Keterangan</td><td><?php echo form_input('keterangan', $pemohon['keterangan'],array('placeholder'=>'keterangan')); ?></td></tr>
    <tr><td>Email</td><td><?php echo form_input('email', $pemohon['email'],array('placeholder'=>'email')); ?></td></tr>
    <tr><td>HP</td><td><?php echo form_input('hp', $pemohon['hp'],array('placeholder'=>'hp')); ?></td></tr>
    <tr><td align="center" colspan="2" >
        <?php echo form_submit('SUBMIT','SIMPAN DATA') ?> 
        <?php echo anchor('pelayanan', 'Kembali')?></td></tr>
</table>

</body>
</html>
<?php echo form_close(); ?> 