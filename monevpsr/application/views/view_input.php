<?php

$this->load->view("header");

?>
<body>
    <div id="top-page">
            <div class="clearfix">
                <div class="home left" style="width:120px;"> 
                    <a href="<?=base_url();?>"> <img src="<?=base_url();?>images/home_5.png"></a> 
                    <a href="<?=base_url();?>" class="text">PU-net</a> 
                </div>
                <div class="home left" style="margin:2px 0 0 0;"> 
                    <a href="http://itv.pu.go.id/"> 
                    <img src="<?=base_url();?>images/puprtv_logo.png" style="width:96px; height:28px;"></a>
                </div>
                <!--div class="language">
                    <a href="" class="bahasa"><img src="<?=base_url();?>images/Inggris-icon.png" title="english" > </a>
                    <a class="bahasa" href=""><img src="<?=base_url();?>images/Indonesia-icon.png" title="indonesia"></a>
                </span></div-->
                
                <div class="time right">
                    <script type="text/javascript" src="<?php echo base_url();?>assets/js/tanggal.js" ></script>
                | <span id="clock"></span>
                 <script type="text/javascript">
                        window.setInterval(function() {
                            var clockspan = document.getElementById("clock");
                            
                            var nowdatetime = new Date();
                            var nowtimestring = nowdatetime.getHours() + ":" + nowdatetime.getMinutes() + ":" + nowdatetime.getSeconds() + " WIB"; 
                            
                            clockspan.innerHTML = nowtimestring;
                        }, 1000);
                    </script> 
                </div>
            </div>
            <!--End .wrap   -->
            
        </div>

        <!-- Menu Sidebar -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <?php echo anchor("pelayanan/dashboard", "<i class='fa fa-dashboard fa-fw'></i> Dashboard"); ?>
                        </li>
                        <li>
                            <?php echo anchor("pelayanan/tabel", "<i class='fa fa-table fa-fw'></i> Tabel"); ?>
                        </li>
                        <li>
                            <?php echo anchor("pelayanan/input", "<i class='fa fa-edit fa-fw'></i> Form Input"); ?>
                        </li>
                    </ul>
                </div>
            </div>

<div id="wrapper">

        <!-- Navigation -->


        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Aplikasi Monev PSR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?=site_url('pelayanan/index');?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            </nav>


             

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Daftar Form</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
    
    
    <h3>Form Monev</h3>
     
    
  
<div class="table-responsive">
    <table class="table table-hover table-condensed">
<!--    <tr><td>No</td><td><?php echo form_input('idpemohon', '',array('placeholder'=>'No')); ?></td></tr>-->

        <tr><td>Propinsi</td><td>
        <?php
        $dd_provinsi_attribute = 'class="form-control select2"';
        echo form_dropdown('idpropinsi',$dd_provinsi, $provinsi_selected, $dd_provinsi_attribute); ?></td></tr>

        <tr><td>Asosiasi</td><td><?php echo form_input('nama', '',array('placeholder'=>'Nama')); ?></td></tr>

    <tr><td>Nama</td><td><?php echo form_input('nama', '',array('placeholder'=>'Nama')); ?></td></tr>
    <tr><td>NIP</td><td><?php echo form_input('nip', '',array('placeholder'=>'NIP')); ?></td></tr>
    
    <tr><td>        
        <div class='form-group'>
    <label>Unit Organisasi</label>
    <td><select name="idsatminkal" class='form-control' id='unor'>
                <option value='0'>--pilih--</option>

    <?php 
    foreach ($unor as $u1) {
        echo "<option value='$u1[idsatminkal]'>$u1[uraiunker]</option>";
    }
    ?>
    </select></td>
    </div>
    </td></tr>        

    <tr><td>
    <div class='form-group'>
    <label>Unit Kerja Eselon 2</label>
    <td> <select name="idunker" class='form-control' id='unker'>
                <option value='0'>--pilih--</option></td>
    </select>
    </div>
    </td></tr>
    
    <tr><td>Eselon 3 / Satker</td><td><?php echo form_input('ideselon3', '',array('placeholder'=>'Eselon 3 atau Satker')); ?></td></tr>
    
    <tr><td>Aplikasi</td><td>
        <?php 
        $dd_aplikasi_attribute = 'class="form-control select2"';
        echo form_dropdown('idaplikasi',$dd_aplikasi, $aplikasi_selected, $dd_aplikasi_attribute); ?>
        </td></tr>
    <tr>
        <td>Masalah</td>
        <td>
        <?php echo form_input('masalah', '',array('placeholder'=>'Judul','size'=>'50')); ?>
        </td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td>
            <textarea class="form-control" rows="2" id="keterangan" name="keterangan" placeholder="Jelaskan Disini"></textarea>
        </td>
    </tr>
    <tr>
        <td>Lampirkan File (.jpg)</td><td>
        <?php if($error){
             echo $error;
        } ?>
 

        <input type="file" name="userfile" size="20" />

    </td>
    </tr>
    <tr><td>Email</td><td><?php echo form_input('email', '',array('placeholder'=>'Email Aktif')); ?></td></tr>
    <tr><td>HP</td><td><?php echo form_input('hp', '',array('placeholder'=>'Nomor HP Aktif')); ?></td></tr>
    <tr>
        <td align="center" colspan="2" >
            <br>
            <input  type="submit" class='btn btn-primary' name="submit" value="SIMPAN DATA"/>
         
            <a href="index" class="btn btn-info" role="button">Kembali</a>
        </td>
    </tr>
</table>
    </div>
    </div>
<?php echo form_close(); ?> 
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

 <!-- jQuery -->
    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('assets/vendor/metisMenu/metisMenu.min.js'); ?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url('assets/vendor/raphael/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/morrisjs/morris.min.js'); ?>"></script>
    <script src="<?php echo base_url('assetsdata/morris-data.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/dist/js/sb-admin-2.js'); ?>"></script>
</body>