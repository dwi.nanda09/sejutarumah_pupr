<?php

$this->load->view("header");

?>
<body>
<div id="top-page">
            <div class="clearfix">
                <div class="home left" style="width:120px;"> 
                    <a href="<?=base_url();?>"> <img src="<?=base_url();?>images/home_5.png"></a> 
                    <a href="<?=base_url();?>" class="text">PU-net</a> 
                </div>
                <div class="home left" style="margin:2px 0 0 0;"> 
                    <a href="http://itv.pu.go.id/"> 
                    <img src="<?=base_url();?>images/puprtv_logo.png" style="width:96px; height:28px;"></a>
                </div>
                <!--div class="language">
                    <a href="" class="bahasa"><img src="<?=base_url();?>images/Inggris-icon.png" title="english" > </a>
                    <a class="bahasa" href=""><img src="<?=base_url();?>images/Indonesia-icon.png" title="indonesia"></a>
                </span></div-->
                
                <div class="time right">
                    <script type="text/javascript" src="<?php echo base_url();?>assets/js/tanggal.js" ></script>
                | <span id="clock"></span>
                 <script type="text/javascript">
                        window.setInterval(function() {
                            var clockspan = document.getElementById("clock");
                            
                            var nowdatetime = new Date();
                            var nowtimestring = nowdatetime.getHours() + ":" + nowdatetime.getMinutes() + ":" + nowdatetime.getSeconds() + " WIB"; 
                            
                            clockspan.innerHTML = nowtimestring;
                        }, 1000);
                    </script> 
                </div>
            </div>
            <!--End .wrap   -->
            
        </div>

<div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=site_url('pelayanan/index');?>">Monev PSR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                
                <!-- /.dropdown -->
                <li class="dropdown"> 
                <?php echo anchor("pelayanan/login", "<i class='fa fa-user fa-fw'></i> login"); ?>

                
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            </nav>


            
                       

        <div id="page-wrapper">
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                    <div class="embed-responsive embed-responsive-16by9">
                      <iframe class="embed-responsive-item" src="http://sigi.pu.go.id/portalpupr/apps/webappviewer/index.html?id=a4ad17d70a524c6fbd03a1ac0444a1c0">
                      </iframe>
                    </div>
                        <div class="panel-heading">
                            <i class="fa fa-table fa-fw"></i> Daftar Pelayanan
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php echo anchor('pelayanan/input','Input Data Pemohon');?>
                            <div class="table-responsive">
                            <style type="text/css">

							    table {
							        table-layout: fixed;
							        word-wrap: break-word;
							    }

							        table th, table td {
							            overflow: hidden;
							        }

							</style>                       
		                   		<table class="table table-striped table-bordered table-hover">
		                        <thead>
		                        <tr><th>No.</th>
		                            <th>Nama Pemohon</th><th>NIP</th><th>Satminkal</th>
		                        <th>Unker</th><th>Eselon3 / Satker</th><th>Propinsi</th>
		                        <th>Aplikasi</th><th>Masalah</th><th>Keterangan</th><th>Email</th><th>HP</th><th>Tanggal</th><th>File</th>
		                        </tr>
		                    	</thead>
		                    	<tbody>
		                        	<?php $no = 1;
		                        	foreach ($pemohon as $p){

		                            echo "<tr>"
		                            . "<td>$no</td>"
		                            //. "<td>$p->idpemohon</td>"
		                                    . "<td>$p->nama</td>
		                                       <td>$p->nip</td>
		                                       <td>$p->satm</td>
		                                       <td>$p->unker</td>
		                                       <td>$p->ideselon3</td>
		                                       <td>$p->uraipropinsi</td>
		                                       <td>$p->apl</td>
		                                       <td>$p->masalah</td>
		                                       <td>$p->keterangan</td>
		                                       <td>$p->email</td> 
		                                       <td>$p->hp</td>
		                                       <td>$p->tanggal</td>
		                                       <td><a href='".base_url()."uploads/".$p->file."'>$p->file";
		                                    echo  "<div class='box-file'>
		                                            <a href='".base_url()."uploads/".$p->file."' target='_blank' class='thumbnail'> <img src=".base_url()."uploads/".$p->file."> </a>
		                                     </div> 
		                                     </td>
		                                    . </tr>";
		                            //echo $b->nama_barang.'<br>';
		                            $no++;
		                        	}
		                        	?>
		                    	</tbody>
		                		</table>
		                	</div>
                       </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    

            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

 <!-- jQuery -->
    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('assets/vendor/metisMenu/metisMenu.min.js'); ?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url('assets/vendor/raphael/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/morrisjs/morris.min.js'); ?>"></script>
    <script src="<?php echo base_url('assetsdata/morris-data.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/dist/js/sb-admin-2.js'); ?>"></script>
</body>