<?php echo form_open_multipart('pelayanan/input_simpan');  ?>
<html>
<head>
	<title>
	 Form Pelayanan Aplikasi
	</title>

	<style>
			* {
			margin:0;
			padding:0;
			}
			body {
			font-family: "Arial", "Helvetica", "Verdana", "sans-serif";
			font-size: 12px;
                        margin-left: 200px;
			}
			img {
			border: none;
			}
			.right {
			float: right;
			}
			.left {
			float: left;
			}
			.clear {
			clear:both;
			}
			.clearfix:after {
			visibility: hidden;
			display: block;
			font-size: 0;
			content: " ";
			clear: both;
			height: 0;
			}
			#top-page {
			background: #0d2d6c;
			border-bottom: 5px solid #ffcc00;
			color: #fefefe;
			padding: 1px 0;
			}
			#top-page a {
			padding-left: 2px;
			}
			#top-page span {
			line-height: 18px;
			font-weight: 300;
			}
			.home {
			width:200px;
			margin:2px 10px 0 10px;
			}
			.home img {
			width:28px;
			height:28px;
			vertical-align: middle;
			}
			.home a {
			font-size:14px;
			font-weight:700;
			text-decoration:none;
			color:#ffca00;
			text-align:middle;
			}
			.home a.text {
			font-size:14px;
			font-weight:700;
			text-decoration:none;
			color:#ffca00;
			text-align:middle;
			}
			.time {
			margin:10px 10px 0 10px;
			}
			.copyright{
			border-top: 5px solid #ffcc00;
			background: #0d2d6c;
			padding: 10px 0;
			text-align: center;
			color: #ffca00;
			margin-top:5px;
                        font-size: 12px;    
			bottom:0;
			
			
			}
			.copyright a{
			font-weight: bold;
			text-decoration: none;
			color: #ffca00;
			padding: 0px 0;
			bottom:10px;
			}
		</style>

<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script>
<!--<script type="text/javascript" src="<?php echo base_url('assets/jam.js') ?>"></script>-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/mainjam.css') ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/style.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/select2/dist/css/select2.css') ?>">
<script src="<?php echo base_url('assets/select2/dist/js/select2.min.js') ?>"> </script>
<script type="text/javascript">
	$(function(){

	$.ajaxSetup({
	type:"POST",
	url: "<?php echo base_url('index.php/select/ambil_data') ?>",
	cache: false,
	});

	$("#unor").change(function(){

	var value=$(this).val();
			if(value>0){
				$.ajax({
			data:{modul:'unker',id:value},
		success: function(respond){
	$("#unker").html(respond);
			}
		});
	}
	});
	});
        
        $(document).ready(function () {
                $(".select2").select2({
                    placeholder: "Please Select"
                });
            });
            
        $(document).ready(function() {
        setInterval(function() {
	     $('#divjam').load('<?php echo base_url('assets/jam.php?acak=') ?>'+ Math.random());
    }, 1000);
  });
	</script>
</head>
<body>  
    <div id="top-page">
			<div class="clearfix">
				<div class="home left" style="width:120px;"> <a href="<?=base_url();?>"> <img src="<?=base_url();?>images/home_5.png"></a> <a href="<?=base_url();?>" class="text">PU-net</a> </div>
				<div class="home left" style="margin:2px 0 0 0;"> <a href="http://itv.pu.go.id/"> <img src="<?=base_url();?>images/puprtv_logo.png" style="width:96px; height:28px;"></a></div>
				<!--div class="language">
					<a href="" class="bahasa"><img src="<?=base_url();?>images/Inggris-icon.png" title="english" > </a>
					<a class="bahasa" href=""><img src="<?=base_url();?>images/Indonesia-icon.png" title="indonesia"></a>
				</span></div-->
				
				<div class="time right">
					<script type="text/javascript" src="<?php echo base_url();?>js/tanggal.js" ></script>
				| <span id="clock"></span> </div>
			</div>
			<!--End .wrap	-->
			
		</div>
    
    <div class="container">
    <h3>Form Pelayanan Aplikasi</h3>
     <div class="jam" style="width: 250px;" align="right">
	<div id="divjam"></div>
	</div>
  
<div class="table-responsive">
    <table class="table table-hover table-condensed">
<!--    <tr><td>No</td><td><?php echo form_input('idpemohon', '',array('placeholder'=>'No')); ?></td></tr>-->
    <tr><td>Nama Pemohon</td><td><?php echo form_input('nama', '',array('placeholder'=>'Nama')); ?></td></tr>
    <tr><td>NIP</td><td><?php echo form_input('nip', '',array('placeholder'=>'NIP')); ?></td></tr>
    
    <tr><td>        
        <div class='form-group'>
	<label>Unit Organisasi</label>
	<td><select name="idsatminkal" class='form-control' id='unor'>
                <option value='0'>--pilih--</option>

	<?php 
	foreach ($unor as $u1) {
		echo "<option value='$u1[idsatminkal]'>$u1[uraiunker]</option>";
	}
	?>
	</select></td>
	</div>
    </td></tr>        

    <tr><td>
	<div class='form-group'>
	<label>Unit Kerja Eselon 2</label>
	<td> <select name="idunker" class='form-control' id='unker'>
                <option value='0'>--pilih--</option></td>
	</select>
	</div>
    </td></tr>
	
    <tr><td>Eselon 3 / Satker</td><td><?php echo form_input('ideselon3', '',array('placeholder'=>'Eselon 3 atau Satker')); ?></td></tr>
    <tr><td>Propinsi</td><td>
        <?php
        $dd_provinsi_attribute = 'class="form-control select2"';
        echo form_dropdown('idpropinsi',$dd_provinsi, $provinsi_selected, $dd_provinsi_attribute); ?></td></tr>
    <tr><td>Aplikasi</td><td>
        <?php 
        $dd_aplikasi_attribute = 'class="form-control select2"';
        echo form_dropdown('idaplikasi',$dd_aplikasi, $aplikasi_selected, $dd_aplikasi_attribute); ?>
        </td></tr>
    <tr>
        <td>Masalah</td>
        <td>
        <?php echo form_input('masalah', '',array('placeholder'=>'Judul','size'=>'50')); ?>
        </td>
    </tr>
    <tr>
        <td>Keterangan</td>
        <td>
            <textarea class="form-control" rows="2" id="keterangan" name="keterangan" placeholder="Jelaskan Disini"></textarea>
        </td>
    </tr>
    <tr>
        <td>Lampirkan File (.jpg)</td><td>
        <?php if($error){
             echo $error;
        } ?>
 

        <input type="file" name="userfile" size="20" />

    </td>
    </tr>
    <tr><td>Email</td><td><?php echo form_input('email', '',array('placeholder'=>'Email Aktif')); ?></td></tr>
    <tr><td>HP</td><td><?php echo form_input('hp', '',array('placeholder'=>'Nomor HP Aktif')); ?></td></tr>
    <tr>
        <td align="center" colspan="2" >
            <br>
            <input  type="submit" class='btn btn-primary' name="submit" value="SIMPAN DATA"/>
         
            <a href="index" class="btn btn-info" role="button">Kembali</a>
        </td>
    </tr>
</table>
    </div>
    </div>
<?php echo form_close(); ?> 
   
   <div class="copyright">
Hak Cipta @ 2017 <a href="#">Kementerian Pekerjaan Umum Dan Perumahan Rakyat Republik Indonesia</a>, All Rights Reserved
    </div>
   
</body>
</html>
