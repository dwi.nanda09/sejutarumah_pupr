<html>
    <head>
        <title>
	 Data Pelayanan Aplikasi
	</title>
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bannerpu.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>">
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/jquery.min.js') ?>"></script
        <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
    <style>
                       .box-file{
                            width: 100%;
                            height: 50px;
                            border: 1px solid;
                            margin: 1%;
                        }
                        .box-file img{
                            width: 100%;                                                 
                        }
                        
		</style>
    </head>
    <body>
            <div id="top-page">
			<div class="clearfix">
				<div class="home left" style="width:120px;"> <a href="<?=base_url();?>"> <img src="<?=base_url();?>images/home_5.png"></a> <a href="<?=base_url();?>" class="text">PU-net</a> </div>
				<div class="home left" style="margin:2px 0 0 0;"> <a href="http://itv.pu.go.id/"> <img src="<?=base_url();?>images/puprtv_logo.png" style="width:96px; height:28px;"></a></div>
				<!--div class="language">
					<a href="" class="bahasa"><img src="<?=base_url();?>images/Inggris-icon.png" title="english" > </a>
					<a class="bahasa" href=""><img src="<?=base_url();?>images/Indonesia-icon.png" title="indonesia"></a>
				</span></div-->
				
				<div class="time right">
					<script type="text/javascript" src="<?php echo base_url();?>js/tanggal.js" ></script>
				| <span id="clock"></span> </div>
			</div>
			<!--End .wrap	-->
			
		</div> 
                <div class="container">
                    <h2><?php echo $judul;?></h2>
                    <p><?php echo anchor('pelayanan/input', 'Input Data Pemohon') ?></p>
                    <div class="table-responsive">
                    <table class="table table-condensed table-hover">
                        <thead>
                        <tr class="success"><th>No.</th>
                            <th>Nama Pemohon</th><th>NIP</th><th>Satminkal</th>
                        <th>Unker</th><th>Eselon3 / Satker</th><th>Propinsi</th>
                        <th>Aplikasi</th><th>Masalah</th><th>Keterangan</th><th>Email</th><th>HP</th><th>Tanggal</th><th>File</th><th colspan="2" ></th></tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($pemohon as $p){

                            echo "<tr class='info'>"
                            . "<td>$no</td>"
                            #. "<td>$p->idpemohon</td>"
                                    . "<td>$p->nama</td>
                                       <td>$p->nip</td>
                                       <td>$p->satm</td>
                                       <td>$p->unker</td>
                                       <td>$p->ideselon3</td>
                                       <td>$p->uraipropinsi</td>
                                       <td>$p->apl</td>
                                       <td>$p->masalah</td>
                                       <td>$p->keterangan</td>
                                       <td>$p->email</td> 
                                       <td>$p->hp</td>
                                       <td>$p->tanggal</td>
                                       <td><a href='".base_url()."uploads/".$p->file."'>$p->file";
                                    echo  "<div class='box-file'>
                                            <a href='".base_url()."uploads/".$p->file."' target='_blank'> <img src=".base_url()."uploads/".$p->file."> </a>
                                     </div> 
                                     </td>
                                    <td>".anchor('pelayanan/edit/'.$p->idpemohon,'EDIT')."</td>
                                    <td>".anchor('pelayanan/delete/'.$p->idpemohon,'DELETE')."</td>
                                    . </tr>";
                            //echo $b->nama_barang.'<br>';
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            </div>
        <div class="copyright">
        Hak Cipta @ 2017 <a href="#">Kementerian Pekerjaan Umum Dan Perumahan Rakyat Republik Indonesia</a>, All Rights Reserved
	</div>
    </body>
</html> 