<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Untitled Document</title>
		<!-- script CSS -->
		<style>
			* {
			margin:0;
			padding:0;
			}
			body {
			font-family: "Arial", "Helvetica", "Verdana", "sans-serif";
			font-size: 12px;
			}
			img {
			border: none;
			}
			.right {
			float: right;
			}
			.left {
			float: left;
			}
			.clear {
			clear:both;
			}
			.clearfix:after {
			visibility: hidden;
			display: block;
			font-size: 0;
			content: " ";
			clear: both;
			height: 0;
			}
			#top-page {
			background: #0d2d6c;
			border-bottom: 5px solid #ffcc00;
			color: #fefefe;
			padding: 1px 0;
			}
			#top-page a {
			padding-left: 2px;
			}
			#top-page span {
			line-height: 18px;
			font-weight: 300;
			}
			.home {
			width:200px;
			margin:2px 10px 0 10px;
			}
			.home img {
			width:28px;
			height:28px;
			vertical-align: middle;
			}
			.home a {
			font-size:14px;
			font-weight:700;
			text-decoration:none;
			color:#ffca00;
			text-align:middle;
			}
			.home a.text {
			font-size:14px;
			font-weight:700;
			text-decoration:none;
			color:#ffca00;
			text-align:middle;
			}
			.time {
			margin:10px 10px 0 10px;
			}
			.copyright{
			border-top: 5px solid #ffcc00;
			background: #0d2d6c;
			padding: 10px 0;
			text-align: center;
			color: #ffca00;
			margin-top:5px;
			float:left;
			bottom:0;
			width:100%;
			
			}
			.copyright a{
			font-weight: bold;
			text-decoration: none;
			color: #ffca00;
			padding: 0px 0;
			bottom:10px;
			}
		</style>
		<!-- end script CSS -->
	</head>
	
	<body>
		<!-- Script HTML Header -->
		<div id="top-page">
			<div class="clearfix">
				<div class="home left" style="width:120px;"> <a href="<?=base_url();?>"> <img src="<?=base_url();?>images/home_5.png"></a> <a href="<?=base_url();?>" class="text">PU-net</a> </div>
				<div class="home left" style="margin:2px 0 0 0;"> <a href="http://itv.pu.go.id/"> <img src="<?=base_url();?>images/puprtv_logo.png" style="width:96px; height:28px;"></a></div>
				<!--div class="language">
					<a href="" class="bahasa"><img src="<?=base_url();?>images/Inggris-icon.png" title="english" > </a>
					<a class="bahasa" href=""><img src="<?=base_url();?>images/Indonesia-icon.png" title="indonesia"></a>
				</span></div-->
				
				<div class="time right">
					<script type="text/javascript" src="<?php echo base_url();?>js/tanggal.js" ></script>
				| <span id="clock"></span> </div>
			</div>
			<!--End .wrap	-->
			
		</div>
		<!--end #top-page-->
		<!-- End Script HTML Header -->
		<!--Script Footer-->
		<div class="copyright">
			Hak Cipta @ 2014 <a href="#">Kementerian Pekerjaan Umum Dan Perumahan Rakyat Republik Indonesia</a>, All Rights Reserved
		</div>
		<!--End Script Footer-->
	</body>
</html>		