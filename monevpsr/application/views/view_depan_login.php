<?php

$this->load->view("header");

?>
<body>
<div id="top-page">
            <div class="clearfix">
                <div class="home left" style="width:120px;"> 
                    <a href="<?=base_url();?>"> <img src="<?=base_url();?>images/home_5.png"></a> 
                    <a href="<?=base_url();?>" class="text">PU-net</a> 
                </div>
                <div class="home left" style="margin:2px 0 0 0;"> 
                    <a href="http://itv.pu.go.id/"> 
                    <img src="<?=base_url();?>images/puprtv_logo.png" style="width:96px; height:28px;"></a>
                </div>
                <!--div class="language">
                    <a href="" class="bahasa"><img src="<?=base_url();?>images/Inggris-icon.png" title="english" > </a>
                    <a class="bahasa" href=""><img src="<?=base_url();?>images/Indonesia-icon.png" title="indonesia"></a>
                </span></div-->
                
                <div class="time right">
                    <script type="text/javascript" src="<?php echo base_url();?>assets/js/tanggal.js" ></script>
                | <span id="clock"></span>
                 <script type="text/javascript">
                        window.setInterval(function() {
                            var clockspan = document.getElementById("clock");
                            
                            var nowdatetime = new Date();
                            var nowtimestring = nowdatetime.getHours() + ":" + nowdatetime.getMinutes() + ":" + nowdatetime.getSeconds() + " WIB"; 
                            
                            clockspan.innerHTML = nowtimestring;
                        }, 1000);
                    </script> 
                </div>
            </div>
            <!--End .wrap   -->
            
        </div>

<div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=site_url('pelayanan/index');?>">Monev PSR</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
            </nav>


            <!-- Menu Sidebar -->
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <form id="frmLogin" class="form-signin" method="POST" action="dashboard">
  <div class="login-wrap">
        <div class="user-login-info">
            <input type="text" name="UserName" class="form-control" placeholder="User ID" autofocus="">
            <input type="password" name="Password" class="form-control" placeholder="Password">
        </div>
        <label class="checkbox">
            <input type="checkbox" name="RememberMe" value="false"> Remember me
        </label>
        <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>
        <label for="message" generated="true" class="error"></label>
    </div>

    

</form>


                        </li>
                        <li>
                            <?php echo anchor("pelayanan/index", "<i class='fa fa-dashboard fa-fw'></i> Daftar Layanan"); ?>
                        </li>
                    </ul>
                </div>
            </div>
                       

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Monev PSR</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-table fa-fw"></i> Daftar Pelayanan
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <?php echo anchor('pelayanan/input','Input Data Pemohon');?>
                            <div class="table-responsive">
                            <style type="text/css">

							    table {
							        table-layout: fixed;
							        word-wrap: break-word;
							    }

							        table th, table td {
							            overflow: hidden;
							        }

							</style>                       
		                   		<table class="table table-striped table-bordered table-hover">
		                        <thead>
		                        <tr><th>No.</th>
		                            <th>Nama Pemohon</th><th>NIP</th><th>Satminkal</th>
		                        <th>Unker</th><th>Eselon3 / Satker</th><th>Propinsi</th>
		                        <th>Aplikasi</th><th>Masalah</th><th>Keterangan</th><th>Email</th><th>HP</th><th>Tanggal</th><th>File</th>
		                        </tr>
		                    	</thead>
		                    	<tbody>
		                        	<?php $no = 1;
		                        	foreach ($pemohon as $p){

		                            echo "<tr>"
		                            . "<td>$no</td>"
		                            //. "<td>$p->idpemohon</td>"
		                                    . "<td>$p->nama</td>
		                                       <td>$p->nip</td>
		                                       <td>$p->satm</td>
		                                       <td>$p->unker</td>
		                                       <td>$p->ideselon3</td>
		                                       <td>$p->uraipropinsi</td>
		                                       <td>$p->apl</td>
		                                       <td>$p->masalah</td>
		                                       <td>$p->keterangan</td>
		                                       <td>$p->email</td> 
		                                       <td>$p->hp</td>
		                                       <td>$p->tanggal</td>
		                                       <td><a href='".base_url()."uploads/".$p->file."'>$p->file";
		                                    echo  "<div class='box-file'>
		                                            <a href='".base_url()."uploads/".$p->file."' target='_blank' class='thumbnail'> <img src=".base_url()."uploads/".$p->file."> </a>
		                                     </div> 
		                                     </td>
		                                    . </tr>";
		                            //echo $b->nama_barang.'<br>';
		                            $no++;
		                        	}
		                        	?>
		                    	</tbody>
		                		</table>
		                	</div>
                       </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    

            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

 <!-- jQuery -->
    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url('assets/vendor/metisMenu/metisMenu.min.js'); ?>"></script>

    <!-- Morris Charts JavaScript -->
    <script src="<?php echo base_url('assets/vendor/raphael/raphael.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/morrisjs/morris.min.js'); ?>"></script>
    <script src="<?php echo base_url('assetsdata/morris-data.js'); ?>"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url('assets/dist/js/sb-admin-2.js'); ?>"></script>
</body>