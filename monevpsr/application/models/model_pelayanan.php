<?php

class model_pelayanan extends CI_Model{
    
    function list_pelayanan(){
		//ambil data aplikasi dari db tabel aplikasi
        $pemohon = $this->db->select('a.*,c.uraiunker satm, b.uraiunker unker, d.uraipropinsi, e.nama apl')
                ->from('pemohon a, unker b, satminkal c, propinsi d, aplikasi e')
                ->where('a.idsatminkal = b.parunker and a.idunker = b.idunker and c.idsatminkal = a.idsatminkal and a.idpropinsi = d.idpropinsi  and e.idaplikasi =a.idaplikasi')
		#$pemohon = $this->db->get('pemohon');
                 ->get('');
		return $pemohon;
	} 
    
    function pelayanan($idpemohon){
            return $this->db->get_where('pemohon',array('idpemohon'=>$idpemohon));
        }
    
    function getPropinsi()
        {
        $propinsi = $this->db->get('provinsi');
        // bikin array
        // please select berikut ini merupakan tambahan saja agar saat pertama
        // diload akan ditampilkan text please select.
        $dd[''] = 'Please Select';
         if ($propinsi->num_rows() > 0){
            foreach ($propinsi->result() as $row)
            {
                // tentukan value (sebelah kiri) dan labelnya (sebelah kanan)
                 $dd[$row->idpropinsi] = $row->uraipropinsi;
            }
         }
         return $dd;
    }
        
        
//    function list_pemohon(){
//		//ambil data pemohon dari db tabel pemohon
//		$pemohon = $this->db->get('pemohon');
//		return $pemohon;
//	}       
    
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

