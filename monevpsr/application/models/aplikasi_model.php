<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
 
class aplikasi_model extends CI_Model
{
    // get data dropdown
    function dd_aplikasi()
    {
        // ambil data dari db
        $this->db->order_by('nama', 'asc');
        $result = $this->db->get('aplikasi');
        
        // bikin array
        // please select berikut ini merupakan tambahan saja agar saat pertama
        // diload akan ditampilkan text please select.
        $dd[''] = 'Pilih Aplikasi';
        if ($result->num_rows() > 0) {
            foreach ($result->result() as $row) {
            // tentukan value (sebelah kiri) dan labelnya (sebelah kanan)
                $dd[$row->idaplikasi] = $row->nama;
            }
        }
        return $dd;
    }
}
 
/* End of file Provinsi_model.php */
/* Location: ./application/models/Provinsi_model.php */