<?php

class layanan extends CI_Controller {
    
    function index(){
        $this->load->model('model_layanan');
        //$layanan = $this->model_layanan->list_layanan()->result(); //convert data dalam bentuk array
        //print_r($barang); //ngecek db dl
        //die; 
        $judul = "Daftar Layanan";
	$data['judul'] = $judul;  //PAssing data
	$data['aplikasi'] = $this->model_layanan->list_layanan()->result();  //convert dalam bentuk objek
	$this->load->view('list_layanan', $data);  //Passing data
    }
    
    function input(){
              $this->load->view('input_layanan');          
        }
        
    function input_simpan(){
            //$idapl = $this->input->post('idaplikasi');
            //echo $idapl;
            $data_aplikasi = array(
                'idaplikasi' => $this->input->post('idaplikasi'),
                'nama'=> $this->input->post('nama'));
            $this->db->insert('aplikasi',$data_aplikasi);
            redirect('layanan');
        }
        
        function edit(){
            $this->load->model('model_layanan');
            $idaplikasi = $this->uri->segment(3);
            $data['aplikasi'] = $this->model_layanan->layanan($idaplikasi)->row_array();
            //$product = $this->model_barang->product($kode_barang)->row_array();
            //print_r($product);
            //die;
            //$this->load->view('edit_barang'); 
            $this->load->view('edit_layanan',$data); //PAssing data
            //echo $this->uri->segment(2);
            //echo "edit";
	}
        
         function edit_simpan(){
            $id            = $this->input->post('id');        //mendapatkan dari form hidden
            $data_aplikasi = array(
                'idaplikasi' => $this->input->post('idaplikasi'),
                'nama'=> $this->input->post('nama'));
            $this->db->where('idaplikasi',$id);
            $this->db->update('aplikasi',$data_aplikasi);
            redirect('layanan');
        }
        
       function delete(){
            $idaplikasi = $this->uri->segment(3);
            $this->db->where('idaplikasi',$idaplikasi);
            $this->db->delete('aplikasi');
            redirect('layanan');
        }
}