<?php

class pelayanan extends CI_Controller {
    
    function index(){
        $this->load->model('model_pelayanan');
        //$layanan = $this->model_layanan->list_layanan()->result(); //convert data dalam bentuk array
        //print_r($barang); //ngecek db dl
        //die; 
        $judul = "Monev PSR";
	   $data['judul'] = $judul;  //PAssing data
	   $data['pemohon'] = $this->model_pelayanan->list_pelayanan()->result();  //convert dalam bentuk objek
        $data['error'] = ' ';

        $this->load->view('view_depan', $data);  //Passing data
	    //$this->load->view('daftar_layanan', $data);  //Passing data
        //$this->load->view('upload_form', array('error' => ' ' ));
//        $this->load->model('models_select');
//        $data['unor']=$this->models_select->unor();
//
//		$this->load->view('input_pelayanan',$data);
    }
    
    function admin(){
        $this->load->model('model_pelayanan');
        //$layanan = $this->model_layanan->list_layanan()->result(); //convert data dalam bentuk array
        //print_r($barang); //ngecek db dl
        //die; 
        $judul = "Daftar Pelayanan";
	$data['judul'] = $judul;  //PAssing data
	$data['pemohon'] = $this->model_pelayanan->list_pelayanan()->result();  //convert dalam bentuk objek
        $data['error'] = ' ';
	$this->load->view('list_pelayanan_admin', $data);  //Passing data
        //$this->load->view('upload_form', array('error' => ' ' ));
//        $this->load->model('models_select');
//        $data['unor']=$this->models_select->unor();
//
//		$this->load->view('input_pelayanan',$data);
    }
    
    function dashboard(){
        $this->load->model('model_pelayanan');
        //$layanan = $this->model_layanan->list_layanan()->result(); //convert data dalam bentuk array
        //print_r($barang); //ngecek db dl
        //die; 
        $judul = "Monev PSR";
        $data['judul'] = $judul;  //PAssing data
        $data['pemohon'] = $this->model_pelayanan->list_pelayanan()->result();  //convert dalam bentuk objek
        $data['error'] = ' ';
       
        $this->load->view('dashboard', $data);  //Passing data

    }

    function tabel(){
        $this->load->model('model_pelayanan');
        //$layanan = $this->model_layanan->list_layanan()->result(); //convert data dalam bentuk array
        //print_r($barang); //ngecek db dl
        //die; 
        $judul = "Monev PSR";
        $data['judul'] = $judul;  //PAssing data
        $data['pemohon'] = $this->model_pelayanan->list_pelayanan()->result();  //convert dalam bentuk objek
        $data['error'] = ' ';
       
        $this->load->view('tabel', $data);  //Passing data

    }

    function input(){
        $this->load->model('models_select');
        $this->load->model('model_pelayanan');
        $this->load->model('provinsi_model');
        $this->load->helper('form_helper');
        $this->load->model('aplikasi_model');
        //$data['unor']=$this->models_select->unor();
        //$data['propinsi']=$this->model_pelayanan->getPropinsi();
        $data = array(
            'dd_provinsi' => $this->provinsi_model->dd_provinsi(),
            'dd_aplikasi' => $this->aplikasi_model->dd_aplikasi(),
            'unor'=>$this->models_select->unor(),
            'provinsi_selected' => $this->input->post('propinsi') ? $this->input->post('propinsi') : '', // untuk edit ganti '' menjadi data dari database misalnya $row->provinsi
            'aplikasi_selected' => $this->input->post('aplikasi') ? $this->input->post('aplikasi') : '', // untuk edit ganti '' menjadi data dari database misalnya $row->provinsi
            'error' => $this->upload->display_errors(),
	);
        $this->load->view('view_input',$data);
        
        }
    
     public function do_upload()
        {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png|pdf';
                $config['max_size']             = 2028;
                $config['max_width']            = 2000;
                $config['max_height']           = 2000;
                
                //echo $config['upload_path'] ;
                
                $this->upload->initialize($config);
                
                $this->load->library('upload',$config);
        }            
        
    function input_simpan(){
            //$idapl = $this->input->post('idaplikasi');
            //echo $idapl;
            $this->load->model('model_pelayanan');
            $data_pemohon = array(
                'idpemohon' => $this->input->post('idpemohon'),
                'nama'=> $this->input->post('nama'),
                'nip' => $this->input->post('nip'),
                'idsatminkal' => $this->input->post('idsatminkal'),
                'idunker' => $this->input->post('idunker'),
                'ideselon3' => $this->input->post('ideselon3'),
                'idpropinsi' => $this->input->post('idpropinsi'),
                'idaplikasi' => $this->input->post('idaplikasi'),
                'masalah' => $this->input->post('masalah'),
                'keterangan' => $this->input->post('keterangan'),
                'file' => $this->input->post('userfile'),
                'email' => $this->input->post('email'),
                'hp' => $this->input->post('hp'),
                'tanggal'=> date("Y-m-d H:i:s"));
            
            $this->do_upload();
            
            if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array ('error' => $this->upload->display_errors());
                      
                        //$this->load->view('upload_form', $error);
                        $this->load->view('input_pelayanan', $error);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $data_pemohon['file'] = $data['upload_data']['file_name'];
                        
                        $judul = "Daftar Pelayanan";
                        $data['judul'] = $judul;  //PAssing data                        
                        $this->db->insert('pemohon',$data_pemohon);
                        $data['pemohon'] = $this->model_pelayanan->list_pelayanan()->result();
                        //$this->load->view('upload_success', $data);
                        $this->notif_email($data_pemohon);
                        //redirect('pelayanan');
                }
            
            //redirect('pelayanan');
        }
        
        function notif_email($email){
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "ssl://smtp.gmail.com"; // kita akan mengirim via akun google
            $config['smtp_port'] = "465";
            $config['smtp_user'] = "data.hanung@gmail.com"; // ini adalah akun yang akan mengirim email
            $config['smtp_pass'] = "bismillaah89"; // ini adalah password akun yang akan mengirim email
            $config['charset'] = "iso-8859-1";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";

            $this->email->initialize($config);

            $tomail = $email; // email tujuan, jika banyak silahkan tambahkan email lagi dengan koma
            $subject = "Notifikasi Email".$email['masalah'];
            $isiEmail = "Terima Kasih telah menginput.\n".
                        "Masalah Anda : ".$email['keterangan']."";

            $this->email->from('data.hanung@gmail.com', 'Email'); // email pengirim
            $this->email->to($email['email']);
           // $this->email->bcc($tomail);
            $this->email->subject($subject);
            $this->email->message($isiEmail);

            if ($this->email->send()){
                echo "berhasil terkirim";
            }else{
                echo $this->email->print_debugger();
            }    
        }
        
        public function tampil_file(){
            $this->data['file'] = $this->db->get('pemohon')->result_array();
            $this->load->view('list_pelayanan',$this->data);
        }
                
        
        function edit(){
            $this->load->model('model_pelayanan');
            $idpemohon = $this->uri->segment(3);
            $data['pemohon'] = $this->model_pelayanan->pelayanan($idpemohon)->row_array();
            $this->load->model('models_select');
            $data['unor']=$this->models_select->unor();
//            //$product = $this->model_barang->product($kode_barang)->row_array();
//            //print_r($product);
//            //die;
//            //$this->load->view('edit_barang'); 
            $this->load->view('edit_pelayanan',$data); //PAssing data
//            //echo $this->uri->segment(2);
//            //echo "edit";
	}
        
         function edit_simpan(){
            $id            = $this->input->post('id');        //mendapatkan dari form hidden
            $data_pemohon = array(
                'idpemohon' => $this->input->post('idpemohon'),
                'nama'=> $this->input->post('nama'),
                'nip' => $this->input->post('nip'),
                'idsatminkal' => $this->input->post('idsatminkal'),
                'idunker' => $this->input->post('idunker'),
                'ideselon3' => $this->input->post('ideselon3'),
                'idpropinsi' => $this->input->post('idpropinsi'),
                'idaplikasi' => $this->input->post('idaplikasi'),
                'masalah' => $this->input->post('masalah'),
                'keterangan' => $this->input->post('keterangan'),
                'email' => $this->input->post('email'),
                'hp' => $this->input->post('hp'));
            $this->db->where('idpemohon',$id);
            $this->db->update('pemohon',$data_pemohon);
            redirect('pelayanan');
        }
//        
       function delete(){
            $idpemohon = $this->uri->segment(3);
            $this->db->where('idpemohon',$idpemohon);
            $this->db->delete('pemohon');
            redirect('pelayanan');
        }
        
        function image_upload(){
            $data[]= "inage ajax Upload";
            $this->load->view('image_upload',$data);
        }

        function login(){
            $this->load->view('login');
        }



}




