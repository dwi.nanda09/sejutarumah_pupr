<?php

Class Select extends CI_Controller

{
	function __construct(){

		parent::__construct();

		$this->load->database();

		$this->load->helper(array('url'));

		$this->load->model('models_select');

	}

	function index(){

		$data['unor']=$this->models_select->unor();

		$this->load->view('input_pelayanan',$data);

	}

	function ambil_data(){
		$modul=$this->input->post('modul');
		
		$id=$this->input->post('id');

		if($modul=="unker"){
		echo $this->models_select->unker($id);
		}
//		else if($modul=="kecamatan"){
//		echo $this->models_select->kecamatan($id);
//
//		}
//		else if($modul=="kelurahan"){
//		echo $this->models_select->kelurahan($id);
//		}
	}
        
        function getAllGroups()
        {
                $query = $this->db->query('SELECT description FROM location');
                
                return $query->result();
        }
        
}
?>