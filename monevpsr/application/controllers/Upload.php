<?php
class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url','file'));
        }

        public function index()
        {
                $this->load->view('upload_form', array('error' => ' ' ));
                //$this->load->view('input_pelayanan', array('error' => ' ' ));
        }

        public function do_upload()
        {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;
                
                echo $config['upload_path'] ;
                
                $this->upload->initialize($config);
                
                $this->load->library('upload',$config);

                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = array ('error' => $this->upload->display_errors());

                        $this->load->view('upload_form', $error);
                        //$this->load->view('input_pelayanan', $data);
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());

                        $this->load->view('upload_success', $data);
                }
        }
}
?>