CREATE SCHEMA SATUJUTARUMAH;

CREATE TABLE SATUJUTARUMAH.provinsi (
  idProv varchar(2) PRIMARY KEY,
  namaProv varchar(50) NOT NULL
);


CREATE TABLE SATUJUTARUMAH.kota (
  idKota varchar(5),
  namaKota TEXT NOT NULL,
  idProv_kota varchar(2) NOT NULL,
  PRIMARY KEY (idKota),
  FOREIGN KEY (idProv_kota) REFERENCES SATUJUTARUMAH.provinsi (idProv) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE SATUJUTARUMAH.akun (
  username varchar(50) PRIMARY KEY,
  password varchar(20) NOT NULL,
  idKota varchar(5) UNIQUE NOT NULL,
  FOREIGN KEY (idKota) REFERENCES SATUJUTARUMAH.kota(idKota) ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE TABLE SATUJUTARUMAH.detail_mbr (
  idKota varchar(5) PRIMARY KEY,
  pengembang int,
  imb int,
  masy int,
  csr int,
  pupr int,
  dak_pupr_pb int,
  dak_pupr_pk int,
  rusunawa_pemda int,
  bsps_pemda_pb int,
  bsps_pemda_pk int,
  rutilahu_kemensos int,
  FOREIGN KEY (idKota) REFERENCES SATUJUTARUMAH.kota(idKota) ON UPDATE CASCADE ON DELETE RESTRICT
);
CREATE TABLE SATUJUTARUMAH.detail_non_mbr (
  idKota varchar(5) PRIMARY KEY,
  pengembangan int,
  imb int,
  masy int,
  FOREIGN KEY (idKota) REFERENCES SATUJUTARUMAH.kota(idKota) ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE SATUJUTARUMAH.total_mbr (
  
);
